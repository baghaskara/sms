-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2018 at 11:15 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sms`
--

-- --------------------------------------------------------

--
-- Table structure for table `bts`
--

CREATE TABLE `bts` (
  `id` int(5) NOT NULL,
  `name_bts` varchar(25) NOT NULL,
  `sector` varchar(15) NOT NULL,
  `region` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daemons`
--

CREATE TABLE `daemons` (
  `Start` text NOT NULL,
  `Info` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gammu`
--

CREATE TABLE `gammu` (
  `Version` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gammu`
--

INSERT INTO `gammu` (`Version`) VALUES
(13);

-- --------------------------------------------------------

--
-- Table structure for table `handling`
--

CREATE TABLE `handling` (
  `id` int(1) NOT NULL,
  `id_ticket` int(5) NOT NULL,
  `action` varchar(20) NOT NULL,
  `action_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inbox`
--

CREATE TABLE `inbox` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ReceivingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Text` text NOT NULL,
  `SenderNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text NOT NULL,
  `SMSCNumber` varchar(20) NOT NULL DEFAULT '',
  `Class` int(11) NOT NULL DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) UNSIGNED NOT NULL,
  `RecipientID` text NOT NULL,
  `Processed` enum('false','true') NOT NULL DEFAULT 'false',
  `statusproceed` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inbox`
--

INSERT INTO `inbox` (`UpdatedInDB`, `ReceivingDateTime`, `Text`, `SenderNumber`, `Coding`, `UDH`, `SMSCNumber`, `Class`, `TextDecoded`, `ID`, `RecipientID`, `Processed`, `statusproceed`) VALUES
('2018-10-22 09:49:02', '2018-10-21 09:25:23', '005400650073', '+6281242050979', 'Default_No_Compression', '', '+6281107908', -1, 'Tes', 1, '', 'false', 0),
('2018-10-22 10:01:28', '2018-10-22 10:01:28', '0054006500730074003100320033', '+628170122234', 'Default_No_Compression', '', '+628184422557', -1, 'Test123', 2, '', 'false', 0),
('2018-10-22 10:04:30', '2018-10-22 10:04:27', '004E007400610070', '+6281586421985', 'Default_No_Compression', '', '+62816124', -1, 'Ntap', 3, '', 'false', 0),
('2018-11-18 05:27:30', '2018-11-18 05:27:27', '004F006B0065002000620072006F002E0020004D0061006E007400610062', '+6281586421932', 'Default_No_Compression', '', '+62816124', -1, 'Oke bro. Mantab', 4, '', 'false', 0),
('2018-11-18 05:27:54', '2018-11-18 05:27:51', '004B006900720069006D002000740065007200750073002000680069006E00670067006100200031003300200053004D0053002000620065007200620061007900610072002000640061006E002000640061007000610074006B0061006E0020006800610072006700610020007300700065007300690061006C002000730065006E0069006C00610069002000520070002000320035002000700065007200200053004D005300200075006E00740075006B00200033003000200053004D005300200062006500720069006B00750074006E00790061002E', 'TELKOMSEL', 'Default_No_Compression', '', '+6281107907', 1, 'Kirim terus hingga 13 SMS berbayar dan dapatkan harga spesial senilai Rp 25 per SMS untuk 30 SMS berikutnya.', 5, '', 'false', 0),
('2018-11-18 05:28:06', '2018-11-18 05:27:51', '004B006900720069006D002000740065007200750073002000680069006E00670067006100200031003300200053004D0053002000620065007200620061007900610072002000640061006E002000640061007000610074006B0061006E0020006800610072006700610020007300700065007300690061006C002000730065006E0069006C00610069002000520070002000320035002000700065007200200053004D005300200075006E00740075006B00200033003000200053004D005300200062006500720069006B00750074006E00790061002E', 'TELKOMSEL', 'Default_No_Compression', '', '+6281107907', 1, 'Kirim terus hingga 13 SMS berbayar dan dapatkan harga spesial senilai Rp 25 per SMS untuk 30 SMS berikutnya.', 6, '', 'false', 0),
('2018-12-02 10:07:40', '2018-12-02 10:00:35', '00530065006C0061006D0061007400200064006100740061006E0067002000640069002000540045004C004B004F004D00530045004C002E00200052006500670069007300740072006100730069006B0061006E0020006B006100720074007500200070007200610062006100790061007200200041006E00640061002000640065006E00670061006E0020006D0065006E0067006900720069006D006B0061006E00200053004D0053002000640065006E00670061006E00200066006F0072006D00610074003A0020005200450047003C00730070006100730069003E004E0049004B0023004E006F004B004B00230020006B006500200034003400340034002000610074006100750020006B0075006E006A0075006E006700690020007400730065006C002E006D0065002F006400610066007400610072', 'TELKOMSEL', 'Default_No_Compression', '', '+6281107907', 1, 'Selamat datang di TELKOMSEL. Registrasikan kartu prabayar Anda dengan mengirimkan SMS dengan format: REG<spasi>NIK#NoKK# ke 4444 atau kunjungi tsel.me/daftar', 7, '', 'false', 0),
('2018-12-02 10:07:40', '2018-12-02 10:00:36', '00530065006C0061006D0061007400200064006100740061006E00670020006400690020004B0061007200740075002000410053002E0020005000550041005300200049006E007400650072006E006500740061006E0020006400690020002A0031003000300023002000260020004E0045004C0050004F004E0020004D00550052004100480020006400690020002A003100300030002A003100300023002C0064006F0077006E006C006F006100640020004D007900540065006C006B006F006D00730065006C0020006400690020007400730065006C002E006D0065002F006D007900740065006C006B006F006D00730065006C002E00200049006E0066006F003A003100380038002E0020004B0061007200740075002000410053002000500061007300200042007500610074002000530065006D007500610021', 'TELKOMSEL', 'Default_No_Compression', '', '+6281107907', 1, 'Selamat datang di Kartu AS. PUAS Internetan di *100# & NELPON MURAH di *100*10#,download MyTelkomsel di tsel.me/mytelkomsel. Info:188. Kartu AS Pas Buat Semua!', 8, '', 'false', 0),
('2018-12-02 10:07:40', '2018-12-02 10:02:53', '00540065007300740020', '+6281586421932', 'Default_No_Compression', '', '+62816124', -1, 'Test ', 9, '', 'false', 0),
('2018-12-02 10:08:03', '2018-12-02 10:08:01', '00540065007300200070006C006500610073006500200062006900730061', '+6281586421932', 'Default_No_Compression', '', '+62816124', -1, 'Tes please bisa', 10, '', 'false', 0),
('2018-12-02 10:10:34', '2018-12-02 10:10:33', '004B006900720069006D002000740065007200750073002000680069006E00670067006100200031003300200053004D0053002000620065007200620061007900610072002000640061006E002000640061007000610074006B0061006E0020006800610072006700610020007300700065007300690061006C002000730065006E0069006C00610069002000520070002000320035002000700065007200200053004D005300200075006E00740075006B00200033003000200053004D005300200062006500720069006B00750074006E00790061002E', 'TELKOMSEL', 'Default_No_Compression', '', '+6281107907', 1, 'Kirim terus hingga 13 SMS berbayar dan dapatkan harga spesial senilai Rp 25 per SMS untuk 30 SMS berikutnya.', 11, '', 'false', 0),
('2018-12-02 10:11:43', '2018-12-02 10:11:41', '00480061006C006F002E0020', '+6281586421932', 'Default_No_Compression', '', '+62816124', -1, 'Halo. ', 12, '', 'false', 0),
('2018-12-02 10:14:11', '2018-12-02 10:14:07', '0049007900610020006D00610073002C002000620069007300610020006E006700670061002000790061003F', '+6281586421932', 'Default_No_Compression', '', '+62816124', -1, 'Iya mas, bisa ngga ya?', 13, '', 'false', 0);

--
-- Triggers `inbox`
--
DELIMITER $$
CREATE TRIGGER `inbox_timestamp` BEFORE INSERT ON `inbox` FOR EACH ROW BEGIN
    IF NEW.ReceivingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.ReceivingDateTime = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(10) NOT NULL,
  `name` text NOT NULL,
  `phone` int(12) NOT NULL,
  `role` text NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `outbox`
--

CREATE TABLE `outbox` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendBefore` time NOT NULL DEFAULT '23:59:59',
  `SendAfter` time NOT NULL DEFAULT '00:00:00',
  `Text` text,
  `DestinationNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text,
  `Class` int(11) DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) UNSIGNED NOT NULL,
  `MultiPart` enum('false','true') DEFAULT 'false',
  `RelativeValidity` int(11) DEFAULT '-1',
  `SenderID` varchar(255) DEFAULT NULL,
  `SendingTimeOut` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `DeliveryReport` enum('default','yes','no') DEFAULT 'default',
  `CreatorID` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Triggers `outbox`
--
DELIMITER $$
CREATE TRIGGER `outbox_timestamp` BEFORE INSERT ON `outbox` FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.SendingDateTime = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingTimeOut = '0000-00-00 00:00:00' THEN
        SET NEW.SendingTimeOut = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `outbox_multipart`
--

CREATE TABLE `outbox_multipart` (
  `Text` text,
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text,
  `Class` int(11) DEFAULT '-1',
  `TextDecoded` text,
  `ID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `SequencePosition` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pbk`
--

CREATE TABLE `pbk` (
  `ID` int(11) NOT NULL,
  `GroupID` int(11) NOT NULL DEFAULT '-1',
  `Name` text NOT NULL,
  `Number` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pbk_groups`
--

CREATE TABLE `pbk_groups` (
  `Name` text NOT NULL,
  `ID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id` int(7) NOT NULL,
  `name` text NOT NULL,
  `id_bts` int(5) NOT NULL,
  `id_ticket` int(5) NOT NULL,
  `number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE `phones` (
  `ID` text NOT NULL,
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TimeOut` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Send` enum('yes','no') NOT NULL DEFAULT 'no',
  `Receive` enum('yes','no') NOT NULL DEFAULT 'no',
  `IMEI` varchar(35) NOT NULL,
  `Client` text NOT NULL,
  `Battery` int(11) NOT NULL DEFAULT '-1',
  `Signal` int(11) NOT NULL DEFAULT '-1',
  `Sent` int(11) NOT NULL DEFAULT '0',
  `Received` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`ID`, `UpdatedInDB`, `InsertIntoDB`, `TimeOut`, `Send`, `Receive`, `IMEI`, `Client`, `Battery`, `Signal`, `Sent`, `Received`) VALUES
('', '2018-12-02 10:14:35', '2018-12-02 10:07:39', '2018-12-02 10:14:45', 'yes', 'yes', '353875040728667', 'Gammu 1.33.0, Windows Server 2007, GCC 4.7, MinGW 3.11', 0, 24, 2, 7);

--
-- Triggers `phones`
--
DELIMITER $$
CREATE TRIGGER `phones_timestamp` BEFORE INSERT ON `phones` FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.TimeOut = '0000-00-00 00:00:00' THEN
        SET NEW.TimeOut = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `problem`
--

CREATE TABLE `problem` (
  `id` int(5) NOT NULL,
  `type` text NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sentitems`
--

CREATE TABLE `sentitems` (
  `UpdatedInDB` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `InsertIntoDB` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `SendingDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DeliveryDateTime` timestamp NULL DEFAULT NULL,
  `Text` text NOT NULL,
  `DestinationNumber` varchar(20) NOT NULL DEFAULT '',
  `Coding` enum('Default_No_Compression','Unicode_No_Compression','8bit','Default_Compression','Unicode_Compression') NOT NULL DEFAULT 'Default_No_Compression',
  `UDH` text NOT NULL,
  `SMSCNumber` varchar(20) NOT NULL DEFAULT '',
  `Class` int(11) NOT NULL DEFAULT '-1',
  `TextDecoded` text NOT NULL,
  `ID` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `SenderID` varchar(255) NOT NULL,
  `SequencePosition` int(11) NOT NULL DEFAULT '1',
  `Status` enum('SendingOK','SendingOKNoReport','SendingError','DeliveryOK','DeliveryFailed','DeliveryPending','DeliveryUnknown','Error') NOT NULL DEFAULT 'SendingOK',
  `StatusError` int(11) NOT NULL DEFAULT '-1',
  `TPMR` int(11) NOT NULL DEFAULT '-1',
  `RelativeValidity` int(11) NOT NULL DEFAULT '-1',
  `CreatorID` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sentitems`
--

INSERT INTO `sentitems` (`UpdatedInDB`, `InsertIntoDB`, `SendingDateTime`, `DeliveryDateTime`, `Text`, `DestinationNumber`, `Coding`, `UDH`, `SMSCNumber`, `Class`, `TextDecoded`, `ID`, `SenderID`, `SequencePosition`, `Status`, `StatusError`, `TPMR`, `RelativeValidity`, `CreatorID`) VALUES
('2018-10-22 09:54:34', '2018-10-22 09:54:26', '2018-10-22 09:54:34', NULL, '0043006F0062006100200073006D0073002000700061006B0065002000440061007400610062006100730065', '085883223034', 'Default_No_Compression', '', '+62816124', -1, 'Coba sms pake Database', 1, '', 1, 'SendingOKNoReport', -1, 237, 255, 'Gammu'),
('2018-10-22 10:04:08', '2018-10-22 10:03:58', '2018-10-22 10:04:08', NULL, '0069006E006900200073006D0073002000670061007400650077006100790020006300750079', '081586421985', 'Default_No_Compression', '', '+62816124', -1, 'ini sms gateway cuy', 2, '', 1, 'SendingOKNoReport', -1, 238, 255, 'Gammu'),
('2018-10-27 21:56:55', '2018-10-27 21:56:46', '2018-10-27 21:56:55', NULL, '0074006500730074002C00200069006E006900200073006D007300200067006100740065007700610079', '08985644808', 'Default_No_Compression', '', '+62816124', -1, 'test, ini sms gateway', 3, '', 1, 'SendingOKNoReport', -1, 241, 255, 'Gammu'),
('2018-11-18 05:26:54', '2018-11-18 05:25:46', '2018-11-18 05:26:54', NULL, '0043006F0062006100200073006D0073002000700061006B0065002000440061007400610062006100730065', '081586421932', 'Default_No_Compression', '', '+6281100000', -1, 'Coba sms pake Database', 4, '', 1, 'SendingOKNoReport', -1, 3, 255, 'Gammu'),
('2018-12-02 10:09:13', '2018-12-02 10:09:07', '2018-12-02 10:09:13', NULL, '0043006F0062006100200073006D0073002000700061006B0065002000440061007400610062006100730065', '081586421932', 'Default_No_Compression', '', '+6281100000', -1, 'Coba sms pake Database', 5, '', 1, 'SendingOKNoReport', -1, 4, 255, 'Gammu'),
('2018-12-02 10:12:47', '2018-12-02 10:12:41', '2018-12-02 10:12:47', NULL, '006D006100750020006B006F006D0070006C00610069006E0020006D00610073003F', '081586421932', 'Default_No_Compression', '', '+6281100000', -1, 'mau komplain mas?', 6, '', 1, 'SendingOKNoReport', -1, 5, 255, 'Gammu');

--
-- Triggers `sentitems`
--
DELIMITER $$
CREATE TRIGGER `sentitems_timestamp` BEFORE INSERT ON `sentitems` FOR EACH ROW BEGIN
    IF NEW.InsertIntoDB = '0000-00-00 00:00:00' THEN
        SET NEW.InsertIntoDB = CURRENT_TIMESTAMP();
    END IF;
    IF NEW.SendingDateTime = '0000-00-00 00:00:00' THEN
        SET NEW.SendingDateTime = CURRENT_TIMESTAMP();
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `id` int(5) NOT NULL,
  `ID_outbox` int(10) NOT NULL,
  `ID_inbox` int(10) NOT NULL,
  `id_problem` int(5) NOT NULL,
  `id_karyawan` int(10) NOT NULL,
  `id_handling` int(1) NOT NULL,
  `id_pelanggan` int(7) NOT NULL,
  `statusproceed` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bts`
--
ALTER TABLE `bts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `handling`
--
ALTER TABLE `handling`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outbox`
--
ALTER TABLE `outbox`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `outbox_date` (`SendingDateTime`,`SendingTimeOut`),
  ADD KEY `outbox_sender` (`SenderID`);

--
-- Indexes for table `outbox_multipart`
--
ALTER TABLE `outbox_multipart`
  ADD PRIMARY KEY (`ID`,`SequencePosition`);

--
-- Indexes for table `pbk`
--
ALTER TABLE `pbk`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pbk_groups`
--
ALTER TABLE `pbk_groups`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`IMEI`);

--
-- Indexes for table `problem`
--
ALTER TABLE `problem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sentitems`
--
ALTER TABLE `sentitems`
  ADD PRIMARY KEY (`ID`,`SequencePosition`),
  ADD KEY `sentitems_date` (`DeliveryDateTime`),
  ADD KEY `sentitems_tpmr` (`TPMR`),
  ADD KEY `sentitems_dest` (`DestinationNumber`),
  ADD KEY `sentitems_sender` (`SenderID`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bts`
--
ALTER TABLE `bts`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inbox`
--
ALTER TABLE `inbox`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `outbox`
--
ALTER TABLE `outbox`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pbk`
--
ALTER TABLE `pbk`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pbk_groups`
--
ALTER TABLE `pbk_groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `problem`
--
ALTER TABLE `problem`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
