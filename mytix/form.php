<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>CSS Registration Form</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="css/default1.css"/>
    </head>
    <body>    
        <form method="post" action="simpan_data.php" name="myForm" class="register">
            <h1>Pendaftaran Pasien Berobat</h1>
            <fieldset class="row2">
                <legend>Personal Details
                </legend>
                <p>
                    <label for="id">ID pelanggan *
                    </label>
                    <input type="text" maxlength="10" name="id" id="id"/>
                </p>
                <p>
                    <label for="name">nama pelanggan *
                    </label>
                    <input type="text" class="long" name="name" id="name"/>
                </p>                
                <p>
                    <label for="id_bts">id bts *
                    </label>
                    <input type="text" maxlength="15" name="id_bts" id="id_bts"/>
                </p>
                <p>
                    <label for="number">nomor pelanggan *
                    </label>
                    <input type="text" class="long" name="number" id="number"/>
                </p>
                </fieldset>
            
                
                <button class="button" type="submit" value="Simpan">Daftar</button>
                <button class="button" type="reset" value="Reset">Reset</button>
                <input class="button" type="button" value="List Pelanggan" onClick="parent.location='pelanggan.php'"/>
                
    </form>
    </body>
</html>
