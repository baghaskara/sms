<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>CSS Registration Form</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="css/default1.css"/>
    </head>
    <body>    
        <form method="post" action="simpan_datakaryawan.php" name="myForm" class="register">
            <h1>Pendaftaran Pasien Berobat</h1>
            <fieldset class="row2">
                <legend>Personal Details
                </legend>
                <p>
                    <label for="id">ID karyawan *
                    </label>
                    <input type="text" maxlength="10" name="id" id="id"/>
                </p>
                <p>
                    <label for="name">Nama Karyawan *
                    </label>
                    <input type="text" class="long" name="name" id="name"/>
                </p>
				<p>
                    <label for="phone">no. telpon *
                    </label>
                    <input type="text" class="long" name="phone" id="phone	""/>
                </p>                
                <p>
                    <label for="role">Role *
                    </label>
					<select name="role" id="role" value="<?php echo $role; ?>">
						<option value="Manageranager">Manager</option>
						<option value="Customer Support">Customer Support</option>
						<option value="NOC">NOC</option>
					</select>
                </p>
                <p>
                    <label for="username">username *
                    </label>
                    <input type="text" class="long" name="username" id="username"/>
                </p>
				<p>
                    <label for="password">Password *
                    </label>
                    <input type="text" class="long" name="password" id="password"/>
                </p>
                </fieldset>
            
                
                <button class="button" type="submit" value="Simpan">Daftar</button>
                <button class="button" type="reset" value="Reset">Reset</button>
                <input class="button" type="button" value="List Karyawan" onClick="parent.location='karyawan.php'"/>
                
    </form>
    </body>
</html>